package running;

import importing.Import;
import importing.ImportParser;
import structuring.Graph;
import structuring.ImportGenerator;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

class Scriptifier {

    private static Logger log = Logger.getLogger("Scriptifier");

    private final ImportParser parser;

    private Scriptifier(ImportParser parser) {
        this.parser = parser;
    }

    void run(URI main, Path outputPath) {
        generateScriptFile(listify(getImportIterator(main)), outputPath);
    }

    private List<Import> listify(Iterator<Import> iterator) {
        log.info("Generating Import list...");
        List<Import> list = new ArrayList<>();
        iterator.forEachRemaining(list::add);
        return list;
    }

    private Iterator<Import> getImportIterator(URI root) {
        log.info("Setting up GraphIterator");
        Graph<Import> graph = new Graph<>(parser.parse(root));
        ImportGenerator graphGenerator = new ImportGenerator(parser);
        return graph.iterator(graphGenerator);
    }

    private void generateScriptFile(List<Import> imports, Path outputPath) {
        List<String> lines = new ArrayList<>();
        lines.addAll(collectImports(imports));
        lines.addAll(collectContent(imports));
        try {
            Files.write(outputPath, lines);
        } catch (IOException ex) {
            log.severe("Couldn't generate script file" + ex);
        }
    }

    private <T1, C extends Collection<T1>> C merge (C a, C b) {
        a.addAll(b);
        return a;
    }

    private List<String> collectImports(List<Import> imports) {
        SortedSet<URI> unresolvedImports = new TreeSet<>();
        Set<URI> resolvedImports = imports.stream()
                .filter(anImport -> !anImport.content.isEmpty())
                .map(anImport -> anImport.location)
                .collect(Collectors.toSet());
        unresolvedImports.addAll(
                imports.stream()
                        .map(anImport -> anImport.imports)
                        .reduce(new HashSet<>(), this::merge)
                        .stream()
                        .filter(outputImport -> !resolvedImports.contains(outputImport))
                        .collect(Collectors.toSet()));
        return unresolvedImports.stream().map(parser::importify).collect(Collectors.toList());
    }

    private List<String> collectContent(List<Import> imports) {
        return imports.stream()
                .map((anImport) -> anImport.content)
                .reduce(new ArrayList<>(), this::merge);
    }

    static class Builder {
        private ImportParser parser;

        Scriptifier build() {
            return new Scriptifier(parser);
        }

        Builder setParser(ImportParser parser) {
            this.parser = parser;
            return this;
        }
    }
}
