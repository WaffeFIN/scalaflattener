package running;

import importing.ScalaImportParser;
import reading.DefaultFileReader;
import reading.FileReader;

import java.net.URI;
import java.nio.file.Paths;
import java.util.EnumMap;
import java.util.Map;

public class Main {

    private enum Parameter {
        SCALA_LOCATION, SCALA_ENTRY, OUTPUT_FILE
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            printUsageInstructions();
            System.exit(1);
        }

        Map<Parameter, String> parameters = getParameters(args);

        FileReader fileReader = new DefaultFileReader(parameters.get(Parameter.SCALA_LOCATION));
        new Scriptifier.Builder()
                .setParser(new ScalaImportParser(fileReader))
                .build()
                .run(URI.create(parameters.get(Parameter.SCALA_ENTRY)), Paths.get(parameters.get(Parameter.OUTPUT_FILE)));
    }

    private static void printUsageInstructions() {
        System.out.println("Arguments (optional): scala_entry (output_file) (scala_location)");
        System.out.println();
        System.out.println("   scala_entry Is the location relative to scala_location of the entry scala file.");
        System.out.println("               The flattening starts here, and won't include any files that are");
        System.out.println("               not necessary for the given scala_entry to function.");
        System.out.println("               Example: running/Main.scala");
        System.out.println("   output_file Optional, default: output.scala. The location relative to the");
        System.out.println("               runtime to where the flattened scala script is saved.");
        System.out.println("scala_location Optional, default: scala. The location relative to the");
        System.out.println("               runtime where the given scala project is contained");
    }

    private static Map<Parameter, String> getParameters(String[] args) {
        Map<Parameter, String> map = new EnumMap<>(Parameter.class);
        map.put(Parameter.SCALA_ENTRY, args[0]);
        map.put(Parameter.OUTPUT_FILE, args.length <= 1? "output.scala": args[1]);
        map.put(Parameter.SCALA_LOCATION, args.length <= 2? "scala": args[2]);
        return map;
    }
}