package reading;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

public interface FileReader {
    List<String> lines(URI uri);

    Set<Path> files(URI uri);
}