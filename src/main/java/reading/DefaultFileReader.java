package reading;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DefaultFileReader implements FileReader {

    private static Logger log = Logger.getLogger("DefaultFileReader");

    private final String scalaLocation;

    public DefaultFileReader(String scalaLocation) {
        this.scalaLocation = scalaLocation;
    }

    @Override
    public List<String> lines(URI uri) {
        try {
            return Files.readAllLines(Paths.get(absolutify(uri)));
        } catch (FileNotFoundException e) {
            log.info("Couldn't find file: " + e);
        } catch (IOException e) {
            log.warning("Couldn't read file from " + uri + ": " + e);
        }
        return Collections.emptyList();
    }

    private URI absolutify(URI uri) {
        return (uri.getScheme() != null && uri.isAbsolute() && !uri.isOpaque())? uri:
            absolutify(uri.toString());
    }

    private URI absolutify(String path) {
        return new File(scalaLocation + File.separator + path).toURI().normalize();
    }

    @Override
    public Set<Path> files(URI uri) {
        try {
            return Files.find(
                    Paths.get(absolutify(uri)),
                    999,
                    (p, bfa) -> bfa.isRegularFile())
                    .collect(Collectors.toSet());
        } catch (FileNotFoundException e) {
            log.info("Couldn't find file: " + e);
        } catch (IOException e) {
            log.warning("Couldn't read files from " + uri + ": " + e);
        }
        return Collections.emptySet();
    }
}
