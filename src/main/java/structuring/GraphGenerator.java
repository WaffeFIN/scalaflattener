package structuring;

import java.util.Set;

public interface GraphGenerator<T> {
    Set<T> generate(GraphNode<T> current);
}
