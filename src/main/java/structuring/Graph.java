package structuring;

import java.util.*;
import java.util.stream.Collectors;

public class Graph<T> {

    private final Map<T, GraphNode<T>> nodeMap;
    private final GraphNode<T> root;

    public Graph(T rootValue) {
        this.root = new GraphNode<>(rootValue);
        this.nodeMap = new HashMap<>();
    }

    private GraphNode<T> getOrPut(T value) {
        if (!nodeMap.containsKey(value))
            nodeMap.put(value, new GraphNode<>(value));
        return nodeMap.get(value);
    }

    public Graph<T> biEdge(T from, T to) {
        GraphNode<T> fromNode = getOrPut(from);
        GraphNode<T> toNode = getOrPut(to);

        fromNode.addDestination(toNode);
        toNode.addSource(fromNode);

        return this;
    }

    public Graph<T> edge(T from, T to) {
        GraphNode<T> fromNode = getOrPut(from);
        GraphNode<T> toNode = getOrPut(to);

        fromNode.addDestination(toNode);

        return this;
    }

    private Collection<GraphNode<T>> getOrPut(Collection<T> identifiers) {
        return identifiers.stream().map(this::getOrPut).collect(Collectors.toSet());
    }

    public Iterator<T> iterator() {
        return new GraphDepthFirstIterator<>(root);
    }

    public Iterator<T> iterator(GraphGenerator<T> generator) {
        return new GraphDepthFirstGeneratingIterator<>(this, generator);
    }

    private static class GraphDepthFirstIterator<T> implements Iterator<T> {

        private final Set<T> processed;
        private final Deque<GraphNode<T>> nodeQueue;

        public GraphDepthFirstIterator(GraphNode<T> root) {
            nodeQueue = new LinkedList<>();
            nodeQueue.add(root);
            processed = new HashSet<>();
        }

        @Override
        public boolean hasNext() {
            return !nodeQueue.isEmpty();
        }

        @Override
        public T next() {
            GraphNode<T> node = nodeQueue.pollLast();
            processed.add(node.getIdentifier());
            nodeQueue.addAll(filterProcessed(queueAdditions(node)));
            return node.getIdentifier();
        }

        private Collection<GraphNode<T>> filterProcessed(Collection<GraphNode<T>> nodes) {
            return nodes.stream()
                    .filter((node) -> !processed.contains(node.getIdentifier()))
                    .collect(Collectors.toSet());
        }

        protected Collection<GraphNode<T>> queueAdditions(GraphNode<T> current) {
            return current.getDestinations();
        }
    }

    private static class GraphDepthFirstGeneratingIterator<T> extends GraphDepthFirstIterator<T> {

        private final Graph<T> graph;
        private final GraphGenerator<T> generator;

        public GraphDepthFirstGeneratingIterator(Graph<T> graph, GraphGenerator<T> generator) {
            super(graph.root);
            this.graph = graph;
            this.generator = generator;
        }

        @Override
        protected Collection<GraphNode<T>> queueAdditions(GraphNode<T> node) {
            Set<T> additions = generator.generate(node);
            additions.forEach(addition -> graph.biEdge(addition, node.getIdentifier()));
            return graph.getOrPut(additions);
        }
    }
}
