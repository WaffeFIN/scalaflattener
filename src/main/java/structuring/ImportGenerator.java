package structuring;

import importing.Import;
import importing.ImportParser;

import java.util.Set;
import java.util.stream.Collectors;

public class ImportGenerator implements GraphGenerator<Import> {

    private final ImportParser parser;

    public ImportGenerator(ImportParser parser) {
        this.parser = parser;
    }

    @Override
    public Set<Import> generate(GraphNode<Import> current) {
        return current.getIdentifier().imports.stream()
                .map(parser::parse)
                .collect(Collectors.toSet());
    }
}
