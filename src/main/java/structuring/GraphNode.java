package structuring;

import java.util.HashSet;
import java.util.Set;

class GraphNode<T> {

    private final T identifier;

    private final Set<GraphNode<T>> sources;
    private final Set<GraphNode<T>> destinations;

    GraphNode(T identifier) {
        this.identifier = identifier;
        this.sources = new HashSet<>();
        this.destinations = new HashSet<>();
    }

    T getIdentifier() {
        return identifier;
    }

    Set<GraphNode<T>> getSources() {
        return sources;
    }

    Set<GraphNode<T>> getDestinations() {
        return destinations;
    }

    void addSource(GraphNode importee) {
        sources.add(importee);
    }

    void addDestination(GraphNode importer) {
        destinations.add(importer);
    }
}
