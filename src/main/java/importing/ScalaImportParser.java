package importing;

import reading.FileReader;

import java.net.URI;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScalaImportParser implements ImportParser {

    private static Logger log = Logger.getLogger("ScalaImportParser");

    private final FileReader fileReader;

    public ScalaImportParser(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    @Override
    public Import parse(URI uri) {
        log.info("Parsing import " + uri);
        Set<URI> imports = new HashSet<>();
        List<String> content = new ArrayList<>();
        String packageName = null;

        List<String> lines = fileReader.lines(uri);
        for (String line : lines) {
            String trimmed = line.trim();
            if (trimmed.startsWith("import")) {
                String importSource = withoutFirstToken(trimmed);
                if (importSource.contains("{")) { //multi-import
                    importSource = importSource.replace("}", "");
                    String[] split = importSource.split("\\{");
                    List<String> files = Arrays.asList(split[1].split(","));
                    files.stream()
                            .map(file -> split[0] + file.trim())
                            .map(this::urify)
                            .flatMap(this::explodeWildcards)
                            .forEach(imports::add);
                } else {
                    imports.addAll(explodeWildcards(urify(importSource)).collect(Collectors.toSet()));
                }
            } else if (trimmed.startsWith("package")) {
                packageName = withoutFirstToken(trimmed);
            } else if (!trimmed.isEmpty()) {
                content.add(line);
            }
        }
        return new Import(uri, packageName, imports, content);
    }

    private String withoutFirstToken(String line) {
        return line.substring(line.indexOf(' ') + 1);
    }

    private URI urify(String importSource) {
        return URI.create(importSource.replaceAll("\\.", "/") + ".scala");
    }

    @Override
    public String importify(URI uri) {
        return "import " + uri.toString()
                .replace("/",".")
                .replaceAll(".scala","");
    }

    private Stream<URI> explodeWildcards(URI uri) {
        String uriString = uri.toString();
        if (!uriString.contains("_"))
            return Stream.of(uri);

        return fileReader.files(URI.create(uriString.split("_")[0])).stream()
                .map(Path::toUri);
    }
}
