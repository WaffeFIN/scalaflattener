package importing;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Import {
    public final URI location;
    public final String packageName;
    public final Set<URI> imports;
    public final List<String> content;

    public Import(URI location, String packageName, Set<URI> imports, List<String> content) {
        this.location = location;
        this.packageName = packageName;
        this.imports = imports;
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Import anImport = (Import) o;
        return Objects.equals(location, anImport.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location);
    }
}
