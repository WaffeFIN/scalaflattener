package importing;

import java.net.URI;

public interface ImportParser {
    Import parse(URI uri);

    String importify(URI uri);
}
